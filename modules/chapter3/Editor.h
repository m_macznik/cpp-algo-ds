#ifndef LINE_EDITOR_H
#define LINE_EDITOR_H

#include <list>
#include <iostream>
#include <fstream>
#include <sstream>
#include <regex>
#include <string>
#include <cstdint>

namespace LineEditor
{
    enum class Command
    {
        NONE = 0,
        INSERT = 1,
        DELETE = 2,
        APPEND = 3,
        LIST = 4,
        EXIT = 5
    };

    class Editor
    {
        public:
            Editor();
            ~Editor() = default;
            Editor(const Editor &) = default;
            Editor(Editor &&) = default;
            Editor &operator=(const Editor &) = default;
            Editor &operator=(Editor &&) = default;

            void ShowWelcome();
            void RunEditor();
        private:
            void ShowPrompt();
            void SetFileName(std::string &&line);
            void WriteToFile();
            void PrintLines();
            void EraseLines();
            void InsertLines(std::string &line);
            void AppendLines(std::string &line);
            void ExecuteRuntimeCommands(LineEditor::Command &cmd, LineEditor::Command &lastCmd, std::string &line);
            void ExecuteDelayedCommands(LineEditor::Command &cmd, std::string &line);
            Command IsCommand(std::string &line);
            std::string             mFileName;
            uint32_t                mFirstArg;
            uint32_t                mSecondArg;
            uint32_t                mCurrLine;
            std::list<std::string>  mText;
    };
}

#endif