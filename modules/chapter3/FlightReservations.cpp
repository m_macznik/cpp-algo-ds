#include "FlightReservations.h"

FlightReservations::Passenger::Passenger(std::string Name) :
    mName(Name) {};

std::string& FlightReservations::Passenger::GetName()
{
    return mName;
}

bool FlightReservations::System::IsTicketReserved(Passenger &passenger)
{
    for (auto pass : mPassengers)
    {
        if (0 == pass.GetName().compare(passenger.GetName()))
        {
            return true; 
        }
    }
    return false;
}

bool FlightReservations::System::ReserveTicket(Passenger &passenger)
{
    if (IsTicketReserved(passenger))
    {
        return false;
    }
    else
    {
        mPassengers.push_back(passenger);
        mPassengers.sort();
        return true;
    }
}

bool FlightReservations::System::CancelReservation(Passenger &passenger)
{
    if (IsTicketReserved(passenger))
    {
        mPassengers.remove(passenger);
        mPassengers.sort();
    }
    return false;
}

void FlightReservations::System::ShowPassengers()
{
    for (auto passenger : mPassengers)
    {
        std::cout << passenger.GetName() << std::endl;
    }
}

std::list<FlightReservations::Passenger>& FlightReservations::System::GetPassengers()
{
    return this->mPassengers;
}