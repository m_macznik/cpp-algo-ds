#ifndef FLIGHT_RESERVATIONS_H
#define FLIGHT_RESERVATIONS_H

#include <iostream>
#include <string>
#include <list>

namespace FlightReservations
{
    class Passenger
    {
        public:
            Passenger() = delete;
            Passenger(std::string Name);
            ~Passenger() = default;
            Passenger(const Passenger &) = default;
            Passenger(Passenger &&) = default;
            Passenger &operator=(Passenger &&) = default;
            bool operator==(const Passenger & p)
            {
                return this->mName == p.mName;
            }
            bool operator<(const Passenger & p)
            {
                return this->mName < p.mName;
            }
            bool operator>(const Passenger & p)
            {
                return this->mName > p.mName;
            }

            std::string &GetName();
        private:
            std::string mName;
    };

    class System
    {
        public:
            System() = default;
            ~System() = default;

            bool IsTicketReserved(Passenger &passenger);
            bool ReserveTicket(Passenger &passenger);
            bool CancelReservation(Passenger &passenger);
            void ShowPassengers();
            std::list<Passenger> &GetPassengers();
        private:
            std::list<Passenger> mPassengers;
    };
}

#endif 