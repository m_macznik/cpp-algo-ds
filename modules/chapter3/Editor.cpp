#include "Editor.h"

LineEditor::Editor::Editor() 
    : mFileName("")
    , mCurrLine(0)
    , mFirstArg(0)
    , mSecondArg(0)
{
}

void LineEditor::Editor::ShowWelcome()
{
    std::cout << "Welcome to simple Line Editor" << std::endl;
    std::cout << "\tWritten by mmacz (marek.macznik@gmail.com)" << std::endl << std::endl;
    std::cout << "Options: " << std::endl;
    std::cout << "I n , where n is the line number, puts the text before given line" << std::endl;
    std::cout << "I , without argument puts the text before current line" << std::endl;
    std::cout << "D n (m optional) , where n and (m) are line numbers, deletes given lines" << std::endl;
    std::cout << "D , without arguments deletes line before current one" << std::endl;
    std::cout << "L n (m optional), prints given line(s) inclusive" << std::endl;
    std::cout << "L , without arguments prints current up to current line" << std::endl;
    std::cout << "A <text> appends <text> to existing lines" << std::endl;
    std::cout << "E save to file and exit" << std::endl;
}

void LineEditor::Editor::ShowPrompt()
{
    std::cout << std::to_string(this->mCurrLine + 1) << "> ";
}

LineEditor::Command LineEditor::Editor::IsCommand(std::string &line)
{
    if (std::regex_match(line, std::regex("I \\d+")))
    {
        this->mFirstArg = std::stoi(line.substr(2, line.size()));
        return Command::INSERT;
    }
    else if (line.compare("I") == 0)
    {
        this->mFirstArg = this->mCurrLine;
        return Command::INSERT;
    }
    else if (line.compare("L") == 0)
    {
        this->mFirstArg = this->mCurrLine;
        this->mSecondArg = 0;
        return Command::LIST;
    }
    else if (std::regex_match(line, std::regex("L \\d+")))
    {
        this->mFirstArg = std::stoi(line.substr(2, line.size()));
        this->mSecondArg = 0;
        return Command::LIST;
    }
    else if (std::regex_match(line, std::regex("L \\d+ \\d+")))
    {
        std::stringstream ss(line.substr(2, line.size()));
        ss >> this->mFirstArg;
        ss >> this->mSecondArg;
        return Command::LIST;
    }
    else if (line.compare("D") == 0)
    {
        this->mFirstArg = this->mCurrLine;
        return Command::DELETE;
    }
    else if (std::regex_match(line, std::regex("D \\d+"))) 
    {
        this->mFirstArg = std::stoi(line.substr(2, line.size()));
        this->mSecondArg = 0;
        return Command::DELETE;
    }
    else if (std::regex_match(line, std::regex("D \\d+ \\d+")))
    {
        std::stringstream ss(line.substr(2, line.size()));
        ss >> this->mFirstArg;
        ss >> this->mSecondArg;
        return Command::DELETE;
    }
    else if (line.compare("A") == 0)
    {
        return Command::APPEND;
    }
    else if (line.compare("E") == 0)
    {
        return Command::EXIT;
    }
    return Command::NONE;
}

void LineEditor::Editor::SetFileName(std::string &&line)
{
    this->mFileName = line;
}

void LineEditor::Editor::WriteToFile()
{
    std::ofstream file;
    file.open(this->mFileName, std::ios::out | std::ios::trunc);
    for (auto text : this->mText)
    {
        file << text << std::endl;
    }
    if (file.is_open()) file.close();
}

void LineEditor::Editor::PrintLines()
{
    int start = !this->mSecondArg ? 0 : this->mFirstArg;
    int end = !this->mSecondArg ? this->mFirstArg : this->mSecondArg;

    auto it = this->mText.begin();
    if (start) for (int i = 0; i < start - 1; i++, it++);
    for (int i = !start ? start : start - 1; i < end; i++, it++)
        std::cout << i + 1 << "# " << it->c_str() << std::endl;
}

void LineEditor::Editor::EraseLines()
{
    int start = !this->mSecondArg ? 0 : this->mFirstArg;
    int end = !this->mSecondArg ? this->mFirstArg : this->mSecondArg;
    
    auto it = this->mText.begin();
    if (start) 
        for (int i = 0; i < start - 1; i++, it++);
    else 
        for (int i = 0; i < end - 1; i++, it++);
    
    if (!start) 
    {
        this->mText.erase(it);
    }
    else
    {
        auto itEnd = it;
        for (int i = 0 ; i < end - 1; i++, itEnd++);
        this->mText.erase(it, itEnd);
    }
    this->mCurrLine = this->mText.size();
}

void LineEditor::Editor::InsertLines(std::string &line)
{
    auto it = this->mText.begin();
    for (int i = 0; i < this->mFirstArg - 1; i++, it++);
    this->mText.insert(it, line);
    this->mCurrLine++;
}

void LineEditor::Editor::AppendLines(std::string &line)
{
    for (auto& text : this->mText)
    {
        text.append(line);
    }
}

void LineEditor::Editor::ExecuteRuntimeCommands(LineEditor::Command &cmd, LineEditor::Command &lastCmd, std::string &line)
{
    if (       (cmd == LineEditor::Command::NONE )
       &&      (lastCmd != LineEditor::Command::INSERT)
       &&      (lastCmd != LineEditor::Command::APPEND)
       )
    {
        this->mCurrLine++;
        this->mText.push_back(line);
    }
    else if (cmd == LineEditor::Command::LIST)
    {
        PrintLines();            
    }
    else if (cmd == LineEditor::Command::DELETE)
    {
        EraseLines();
    }
}

void LineEditor::Editor::ExecuteDelayedCommands(LineEditor::Command &cmd, std::string &line)
{
    if (cmd == Command::INSERT)
    {
        Editor::InsertLines(line);
    }
    else if (cmd == Command::APPEND)
    {
        Editor::AppendLines(line);
    }
}

void LineEditor::Editor::RunEditor()
{
    std::string line;
    LineEditor::Command cmd = LineEditor::Command::NONE;
    LineEditor::Command lastCmd = cmd;

    while(!std::regex_match(line, std::regex("EDIT \\S+")))
    {
        std::cout << "To start type: EDIT <filename>" << std::endl;
        std::getline(std::cin, line);
        if (line.compare("E") == 0) return;
    }
    SetFileName(line.substr(5, line.size())); // 5 is the command offset

    while(true)
    {
        ShowPrompt();
        std::getline(std::cin, line);
        cmd = IsCommand(line);
        if (cmd == LineEditor::Command::EXIT) break;
        ExecuteRuntimeCommands(cmd, lastCmd, line);
        ExecuteDelayedCommands(lastCmd, line);
        lastCmd = cmd;
    }
    WriteToFile();
}