#ifndef __FUNCS_H
#define __FUNCS_H

#include <iostream>
#include <string>
#include <sstream>

template<class T>
class Quaternion
{
    private:
        T r = 0;
        T i = 0;
        T j = 0;
        T k = 0;

    public:

        Quaternion() {}

        Quaternion(T r, T i, T j, T k)
        {
            this->r = r;
            this->i = i;
            this->j = j;
            this->k = k;
        }

        Quaternion(const Quaternion &q)
        {
            this->r = q.r;
            this->i = q.i;
            this->j = q.j;
            this->k = q.k;
        }

        std::string ToString()
        {
            std::ostringstream oss;
            if (0 < this->r)
                oss << this->r;
            else if (0 > this->r)
                oss << "-" << this->r;
            
            if (0 < this->i)
                oss << "+" << this->i << "i";
            else if (0 > this->i)
                oss << this->i << "i";

            if (0 < this->j)
                oss << "+" << this->j << "j";
            else if (0 > this->j)
                oss << this->j << "j";

            if (0 < this->k)
                oss << "+" << this->k << "k";
            else if (0 > this->k)
                oss << this->k << "k";

            return oss.str();
        }

        bool Compare(Quaternion &q)
        {
            std::string s1 = this->ToString();
            std::string s2 = q.ToString();
            return s1.compare(s2) == 0 ? true : false;
        }

        Quaternion operator+(const Quaternion &q)
        {
            Quaternion d;
            d.r = this->r + q.r;
            d.i = this->i + q.i;
            d.j = this->j + q.j;
            d.k = this->k + q.k;
            return d;
        }

        void operator+=(const Quaternion &q)
        {
            this->r += q.r;
            this->i += q.i;
            this->j += q.j;
            this->k += q.k;
        }

        Quaternion operator-(const Quaternion &q)
        {
            Quaternion d;
            d.r = this->r - q.r;
            d.i = this->i - q.i;
            d.j = this->j - q.j;
            d.k = this->k - q.k;
            return d;
        }

        void operator-=(const Quaternion &q)
        {
            this->r -= q.r;
            this->i -= q.i;
            this->j -= q.j;
            this->k -= q.k;
        }

        Quaternion operator*(const Quaternion &q)
        {
            Quaternion d;

            d.r = this->r * q.r - 
                  this->i * q.i -
                  this->j * q.j -
                  this->k * q.k;

            d.i = this->r * q.i +
                  this->i * q.r +
                  this->j * q.k -
                  this->k * q.j;

            d.j = this->r * q.j +
                  this->j * q.r +
                  this->k * q.i -
                  this->i * q.k;

            d.k = this->r * q.k +
                  this->k * q.r +
                  this->i * q.j -
                  this->j * q.i;

            return d;
        }
};

#endif /* __FUNCS_H */
