#ifndef __DL_LIST_H
#define __DL_LIST_H

#include <sstream>
#include <string>

template<typename T>
class DLNode
{
    public:
        T item;
        class DLNode *next;
        class DLNode *prev;
        DLNode(T item, DLNode *next = NULL, DLNode *prev = NULL)
        {
            this->item = item;
            this->next = next;
            this->prev = prev;
        }
};

template<typename T>
class DLList
{
    private:
        DLNode<T> *head;
        DLNode<T> *tail;
    public:
        DLList()
        {
            this->head = NULL;
            this->tail = NULL;
        }
        
        bool isEmpty()
        {
            return NULL == this->head;
        }

        ~DLList()
        {
            for(DLNode<T> *pNode; !this->isEmpty();)
            {
                pNode = this->head->next;
                delete this->head;
                this->head = pNode;
            }
        }

        void insert(T item)
        {
            if (NULL == this->head)
            {
                this->head = new DLNode<T>(item);
                this->tail = this->head;
            }
            else
            {
                DLNode<T> *pNode = new DLNode<T>(item);
                DLNode<T> *lastHead = this->head;
                this->head = pNode;
                this->head->next = lastHead;
                lastHead->prev = this->head;
            }
        }

        void append(T item)
        {
            if (NULL == this->tail)
            {
                this->head = this->tail = new DLNode<T>(item);
            }
            else
            {
                this->tail->next = new DLNode<T>(item);
                this->tail->next->prev = this->tail;
                this->tail = this->tail->next;
            }
        }

        std::string toString(int element)
        {
            std::ostringstream oss;
            if (!this->isEmpty())
            {
                DLNode<T> *pNode = this->head;
                for(
                        int i = 1;
                        i < element;
                        ++i, pNode = pNode->next
                   );
                oss << pNode->item;
            }
            return oss.str();
        }

        T pop()
        {
            if (!this->isEmpty())
            {
                T item = this->head->item;
                DLNode<T> *pNode = this->head;
                if (this->head == this->tail)
                {
                    this->tail->next = NULL;
                    this->tail->prev = NULL;
                    this->tail = NULL;
                    this->head = this->tail;
                }
                else
                {
                    this->head = this->head->next;
                    this->head->prev = NULL;
                }
                delete pNode;
                return item;
            }
            return 0;
        }

        T drop()
        {
            if (!this->isEmpty())
            {
                T item = this->tail->item;
                if (this->head == this->tail)
                {
                    delete this->head;
                    this->head = this->tail = NULL;
                }
                else
                {
                    DLNode<T> *pNode = this->tail->prev;
                    pNode->next = NULL;
                    delete this->tail;
                    this->tail = pNode;
                }
                return item;
            }
            return 0;
        }

        bool isInList(T item)
        {
            if (!this->isEmpty())
            {
                DLNode<T>* pNode;
                for(
                        pNode = this->head;
                        (pNode != NULL) && !(pNode->item == item);
                        pNode = pNode->next
                   );
                return pNode != NULL;
            }
            return false;
        }

        void printAll()
        {
            for(
                    DLNode<T> *pNode = this->head;
                    pNode != NULL;
                    pNode = pNode->next
               )
            {
                std::cout << pNode->item << std::endl;
            }
        }

};

#endif /* __DL_LIST_H */
