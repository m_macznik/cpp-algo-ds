#ifndef __SL_LIST
#define __SL_LIST

#include <string>
#include <sstream>

template<typename T>
class SLNode 
{
    public:
        class SLNode *next;
        T item;
        SLNode(T item, SLNode *next = NULL)
        {
            this->item = item;
            this->next = next;
        }
};

template<typename T>
class SLList
{
    private:
        SLNode<T> *head;
        SLNode<T> *tail;

    public:
        SLList()
        {
            this->head = NULL;
            this->tail = NULL;
        }

        ~SLList() 
        {
            for (SLNode<T> *pNode; !this->isEmpty();)
            {
                pNode = this->head->next;
                delete this->head;
                this->head = pNode;
            }
        };

        bool isEmpty()
        {
            return NULL == this->head;
        }

        void insert(T item)
        {
            if (NULL == this->head)
            {
                this->head = new SLNode<T>(item);
                this->tail = this->head;
            }
            else
            {
                SLNode<T> *pNode = new SLNode<T>(item);
                SLNode<T> *pTmp = this->head;
                this->head = pNode;
                this->head->next = pTmp;
            }
        }

        void append(T item)
        {
            if (NULL != this->tail)
            {
                this->tail->next = new SLNode<T>(item);
                this->tail = this->tail->next;
            }
            else
            {
                this->tail = new SLNode<T>(item);
                this->head = this->tail;
            }
        }

        T pop()
        {
            if (!this->isEmpty())
            {
                T item = this->head->item;
                SLNode<T> *tmp = this->head;
                if (this->head == this->tail)
                {
                    this->tail = NULL;
                    this->head = this->tail;
                }
                else 
                {
                    this->head = this->head->next;
                }
                delete tmp;
                return item;
            }
            return 0;
        }

        T drop()
        {
            if (!this->isEmpty())
            {
                T item = this->tail->item;
                if (this->head == this->tail)
                {
                    delete this->head;
                    this->head = this->tail = NULL;
                }
                else
                {
                    SLNode<T>* pNode;
                    for ( 
                            pNode = this->head;
                            pNode->next != this->tail;
                            pNode = pNode->next
                        ); 
                    delete this->tail;
                    this->tail = pNode;
                    this->tail->next = NULL;
                }
                return item;
            }
            else
            {
                return 0;
            }
        }

        bool isInList(T item)
        {
            if (!this->isEmpty())
            {
                SLNode<T>* pNode;
                for(
                        pNode = this->head;
                        (pNode != NULL) && !(pNode->item == item);
                        pNode = pNode->next
                   );
                return pNode != NULL;
            }
            return false;
        }

        std::string toString(int element)
        {
            std::ostringstream oss;
            SLNode<T> *pNode = this->head;
            for(
                    int i = 1;
                    i < element;
                    ++i, pNode = pNode->next
               );
            oss << "Node value: " << pNode->item;
            return oss.str();
        }
};

#endif /* __SL_LIST */
