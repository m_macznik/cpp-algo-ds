#include "FlightReservations.h"
#include <gtest/gtest.h>
#include <list>
#include <array>
#include <string>

TEST(FlightReservations, ProperReservations)
{
    FlightReservations::System system;
    std::array<std::string, 4> arrNames = { "Alice", "Bob", "Clive", "Dumbo" };
    std::list<FlightReservations::Passenger> myList;
    for (auto name : arrNames)
        myList.push_back(FlightReservations::Passenger(name));

    for (auto passenger : myList)
        system.ReserveTicket(passenger);

    auto passengersList = system.GetPassengers();
    int i = 0;
    ASSERT_EQ(passengersList.size(), arrNames.size());
    for (auto passenger : passengersList)
    {
        EXPECT_EQ(passenger.GetName(), arrNames[i]);
        i++;
    }
}

TEST(FlightReservations, ReserveTwoCancelOne)
{
    std::array<std::string, 2> arrNames = {"Alice", "Bob"};
    FlightReservations::System system;
    std::list<FlightReservations::Passenger> inputList;
    for (auto name : arrNames)
        inputList.push_back(FlightReservations::Passenger(name));

    for (auto passenger : inputList)
        system.ReserveTicket(passenger);
    
    ASSERT_EQ(arrNames.size(), system.GetPassengers().size());
    system.CancelReservation(system.GetPassengers().back());
    EXPECT_EQ(arrNames.size() - 1, system.GetPassengers().size());
}

TEST(FlightReservations, ReserveTwoCheckReservations)
{
    std::array<std::string, 2> arrNames = {"Alice", "Bob"};
    FlightReservations::System system;
    std::list<FlightReservations::Passenger> inputList;
    for (auto name : arrNames)
        inputList.push_back(FlightReservations::Passenger(name));

    for (auto passenger : inputList)
        system.ReserveTicket(passenger);

    for (auto passenger : system.GetPassengers())
        EXPECT_TRUE(system.IsTicketReserved(passenger));
}