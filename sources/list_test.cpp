#include <gtest/gtest.h>
#include <sl_list.h>
#include <dl_list.h>

TEST(SinglyLinkedList, list_is_empty)
{
    SLList<int> sl_list;
    ASSERT_TRUE(sl_list.isEmpty());
}

TEST(SinglyLinkedList, insert_one_element_and_list_not_empty)
{
    SLList<int> sl_list;
    sl_list.insert(1);
    ASSERT_FALSE(sl_list.isEmpty());
}

TEST(SinglyLinkedList, append_one_element_and_list_not_empty)
{
    SLList<int> sl_list;
    sl_list.append(1);
    ASSERT_FALSE(sl_list.isEmpty());
}

TEST(SinglyLinkedList, append_one_element_one_pop_list_empty)
{
    SLList<int> sl_list;
    int element;
    sl_list.append(1);
    element = sl_list.pop();
    ASSERT_TRUE(sl_list.isEmpty());
    ASSERT_TRUE(1 == element);
}

TEST(SinglyLinkedList, append_two_elements_one_pop_list_not_empty)
{
    int element;
    SLList<int> sl_list;
    sl_list.append(1);
    sl_list.append(2);
    element = sl_list.pop();
    ASSERT_FALSE(sl_list.isEmpty());
    ASSERT_TRUE(1 == element);
}

TEST(SinglyLinkedList, pop_from_empty_list)
{
    SLList<int> sl_list;
    ASSERT_TRUE(0 == sl_list.pop());
    ASSERT_TRUE(sl_list.isEmpty());
}

TEST(SinglyLinkedList, drop_last_element_from_empty_list)
{
    SLList<int> sl_list;
    ASSERT_TRUE(0 == sl_list.drop());
}

TEST(SinglyLinkedList, append_two_ints_drop_last)
{
    SLList<int> sl_list;
    int last;
    sl_list.append(1);
    sl_list.append(2);
    last = sl_list.drop();
    ASSERT_TRUE(2 == last);
}

TEST(SinglyLinkedList, append_three_ints_drop_last_pop_first)
{
    SLList<int> sl_list;
    int last; 
    int first;
    sl_list.append(1);
    sl_list.append(2);
    sl_list.append(3);
    last = sl_list.drop();
    first = sl_list.pop();
    ASSERT_TRUE(3 == last);
    ASSERT_TRUE(1 == first);
}

TEST(SinglyLinkedList, append_three_drop_insert_pop)
{
    SLList<int> sl_list;
    int last;
    (void)last;
    int first;
    sl_list.append(1);
    sl_list.append(2);
    sl_list.append(3);
    last = sl_list.drop();
    sl_list.insert(4);
    first = sl_list.pop();
    ASSERT_TRUE(4 == first);
    ASSERT_TRUE(3 == last);
}

TEST(SinglyLinkedList, append_four_check_if_second_exist)
{
    SLList<int> sl_list;
    sl_list.append(1);
    sl_list.append(2);
    sl_list.append(3);
    sl_list.append(4);
    ASSERT_TRUE(sl_list.isInList(2));
}

TEST(SinglyLinkedList, append_three_get_second_item_string)
{
    SLList<int> sl_list;
    std::string str;
    sl_list.append(1);
    sl_list.append(2);
    sl_list.append(3);
    str = sl_list.toString(2);
    ASSERT_TRUE(0 == str.compare("Node value: 2"));
}

TEST(DoublyLinkedList, create_node_check_if_empty)
{
    DLNode<int> dlnode(0);
    ASSERT_TRUE(NULL == dlnode.prev);
    ASSERT_TRUE(NULL == dlnode.next);
}

TEST(DoublyLinkedList, create_list_instance_check_if_empty)
{
    DLList<int> dl_list;
    ASSERT_TRUE(dl_list.isEmpty());
}

TEST(DoublyLinkedList, insert_two_ints_check_first)
{
    DLList<int> dl_list;
    std::string item;
    dl_list.insert(1);
    dl_list.insert(2);
    item = dl_list.toString(1);
    ASSERT_TRUE(0 == item.compare("2"));
}

TEST(DoublyLinkedList, append_one_check_item)
{
    DLList<int> dl_list;
    std::string item;
    dl_list.append(1);
    item = dl_list.toString(1);
    ASSERT_TRUE(0 == item.compare("1"));
}

TEST(DoublyLinkedList, append_two_check_last)
{
    DLList<int> dl_list;
    std::string item;
    dl_list.append(1);
    dl_list.append(2);
    item = dl_list.toString(2);
    ASSERT_TRUE(0 == item.compare("2"));
}

TEST(DoublyLinkedList, append_two_ints_pop_one)
{
    DLList<int> dl_list;
    int item;
    dl_list.append(1);
    dl_list.append(2);
    item = dl_list.pop();
    ASSERT_TRUE(1 == item);
}

TEST(DoublyLinkedList, append_two_pop_one_insert_one_int)
{
    DLList<int> dl_list;
    std::string item;
    dl_list.append(1);
    dl_list.append(2);
    dl_list.pop();
    dl_list.insert(3);
    item = dl_list.toString(1);
    ASSERT_TRUE(0 == item.compare("3"));
}

TEST(DoublyLinkedList, append_three_ints_drop_one)
{
    DLList<int> dl_list;
    int item;
    dl_list.append(1);
    dl_list.append(2);
    dl_list.append(3);
    item = dl_list.drop();
    ASSERT_TRUE(3 == item);
}

TEST(DoublyLinkedList, append_three_ints_check_if_second_is_in_list)
{
    DLList<int> dl_list;
    dl_list.append(1);
    dl_list.append(2);
    dl_list.append(3);
    ASSERT_TRUE(dl_list.isInList(2));
}
