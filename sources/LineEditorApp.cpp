#include "Editor.h"

int main(int argc, char **argv)
{
    LineEditor::Editor edit;
    edit.ShowWelcome();
    edit.RunEditor();
    return 0;
}