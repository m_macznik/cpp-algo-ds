#include <gtest/gtest.h>
#include "Quaternions.h"

TEST(Quaternion, to_string)
{
    Quaternion<int> q1(1, 2, 3, 4);
    std::string qstr = q1.ToString();
    ASSERT_TRUE(0 == qstr.compare(q1.ToString()));
}

TEST(Quaternion, comparison)
{
    Quaternion<int> q1(1, 2, 3, 4);
    Quaternion<int> q2 = q1;
    Quaternion<int> q3(1, 3, 4, 2);
    ASSERT_TRUE(q1.Compare(q2));
    ASSERT_FALSE(q1.Compare(q3));
}

TEST(Quaternion, add)
{
    Quaternion<int> q1(1, 2, 3, 4);
    Quaternion<int> q2(4, 3, 2, 1);
    Quaternion<int> exp_result(5, 5, 5, 5);
    Quaternion<int> result = q1 + q2;
    ASSERT_TRUE(result.Compare(exp_result));
    q1 += q2;
    ASSERT_TRUE(q1.Compare(exp_result));
}

TEST(Quaternion, subtract)
{
    Quaternion<int> q1(1, 2, 3, 4);
    Quaternion<int> q2(4, 3, 2, 1);
    Quaternion<int> exp_result(-3, -1, 1, 3);
    Quaternion<int> result = q1 - q2;
    ASSERT_TRUE(result.Compare(exp_result));
    q1 -= q2;
    ASSERT_TRUE(q1.Compare(exp_result));
}

TEST(Quaternion, multiply)
{
    Quaternion<int> q1(1, -2, 3, -4);
    Quaternion<int> q2(-2, 1, -4, -3);
    Quaternion<int> exp_result(0, -20, -20, 10);
    Quaternion<int> result = q1 * q2;
    ASSERT_TRUE(result.Compare(exp_result));
}
